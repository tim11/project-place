'use strict';

var
	gulp = require("gulp"),
    browserSync = require('browser-sync').create(),
    sass = require("gulp-sass"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    cleanCSS = require('gulp-clean-css'),
    rigger = require('gulp-rigger'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    uncss = require('gulp-uncss'),
    wiredep = require('wiredep').stream,
    uglifycss = require('gulp-uglifycss');


var
    compress  = false,
    production = false;

var settings = {
    htmlSrc: "source/html",
    start: "public",
    sass: {
        path: "source/sass",
        start: "source/sass/index.scss"
    },
    css: {
        path: "public/assets/style",
        production: "public/assets/style",
        name: "style",
        suffix:  ".min"
    },
    js: {
        path: "source/script",
        start: "source/script/developer.js",
        production: "public/assets/script",
        name: "main",
        suffix: ".min"
    },
    bower: {
    	file: "./bower.json",
    	path: "./public/assets/bower/"
    }
};
gulp.task('start-server', ['sass', 'js', 'html'], function() {

    browserSync.init({
        server: settings.start,
        port: 4000
    });
    gulp
        .watch(
            [
                settings.htmlSrc + "/*.html",
                settings.htmlSrc + "/**/*.html"
            ], ["html"]
        );
    gulp
        .watch(
            [
                settings.sass.path + "/*.scss",
                settings.sass.path + "/**/*.scss"
            ], ["sass"]
        );
    gulp
        .watch(
            [
                settings.js.start
            ], ["js"]
        );
	gulp
        .watch(
            [
                settings.bower.file
            ], ["html"]
        );
});

gulp.task("sass", function(){
    var file = gulp
        .src(settings.sass.start)
        .pipe(plumber())
        .pipe(
            sass().on('error', sass.logError)
        )
        .pipe(
            autoprefixer({
                browsers: ["last 15 versions", "> 1%", "ie 9"],
            })
        )
        .pipe(
            rename({
                basename: settings.css.name
            })
        )
        .pipe(gulp.dest(settings.css.path));
    if (compress){
        file
            .pipe(
                cleanCSS({
                    compatibility: 'ie8'
                })
            )
            .pipe(
                rename({
                    basename: settings.css.name,
                    suffix: settings.css.suffix
                })
            )
            .pipe(gulp.dest(settings.css.path));
    }
    if (production){
        file
        	.pipe(uncss({
    			html: [
					settings.htmlSrc + "/*.html",
                	settings.htmlSrc + "/**/*.html"
    			]
  			}))
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(
                rename({
                    basename: settings.css.name,
                    suffix: settings.css.suffix
                })
            )
            .pipe(
                gulp.dest(settings.css.production)
            );
    }

    if (browserSync)
        file.
            pipe(
                browserSync.stream()
        );
});

gulp.task("js", function(){
    var file = gulp
        .src(settings.js.start)
        .pipe(plumber())
        .pipe(
            rename({
                basename: settings.js.name
            })
        )
        .pipe(gulp.dest(settings.js.production));
    if (compress){
        file
            .pipe(uglify({
                mangle: true
            }))
            .pipe(
                rename({
                    basename: settings.js.name,
                    suffix: settings.js.suffix
                })
            )
            .pipe(gulp.dest(settings.js.production));
    }
    if (browserSync)
        browserSync.reload();
});

gulp.task("html", function(){
    gulp
    	.src(settings.htmlSrc+'/*.html')
    	.pipe(plumber())
        .pipe(rigger())
		.pipe(
        	wiredep({
                bowerJson: settings.bower.file,
        		directory: settings.bower.path
        	})
        )
        .pipe(gulp.dest(settings.start));
    if (browserSync)
        browserSync.reload();

});

gulp.task("default", ["start-server"]);

gulp.task("compress", function(){
    production = true;
    compress = true;
    gulp.run("html");
    gulp.run("sass");
    gulp.run("js");
})